"""
# Ciclo 01:
# Teste
import unittest

class TestEmailVerification(unittest.TestCase):
    def test_verificar_email_existente(self):
        resultado = verificar_email('usuario_existente@example.com')
        self.assertTrue(resultado)

# Implementação
def verificar_email(email):
    # Implementação para verificar se o email existe
    # (Exemplo fictício: consideramos que o e-mail existe se estiver na lista)
    lista_de_emails = ['usuario_existente@example.com']
    return email in lista_de_emails

# Refatoração
def enviar_email(destinatario, assunto, corpo):
    try:
        # Implementação para enviar o e-mail
        print(f"Enviando e-mail para {destinatario} - Assunto: {assunto} - Corpo: {corpo}")
        return True  # Indicando que o e-mail foi enviado com sucesso
    except Exception as e:
        print(f"Erro ao enviar e-mail: {e}")
        return False  # Indicando que houve um erro ao enviar o e-mail

# Ciclo 02:
# Teste
class TestEmailVerification(unittest.TestCase):
    # Ciclo 1
    def test_verificar_email_existente(self):
        resultado = verificar_email('usuario_existente@example.com')
        self.assertTrue(resultado)

    # Ciclo 2
    def test_verificar_email_com_sucesso(self):
        resultado = enviar_email('usuario@example.com', 'Assunto', 'Corpo do email')
        self.assertTrue(resultado)

if __name__ == '__main__':
    unittest.main()

# Implementação
def enviar_email(destinatario, assunto, corpo):
    # Configurações para o servidor SMTP (exemplo para o Gmail)
    servidor_smtp = 'smtp.gmail.com'
    porta_smtp = 587
    remetente = 'seu_email@gmail.com'  # Substitua pelo seu e-mail
    senha = 'sua_senha'  # Substitua pela sua senha

    # Criando o corpo do e-mail
    mensagem = MIMEMultipart()
    mensagem['From'] = remetente
    mensagem['To'] = destinatario
    mensagem['Subject'] = assunto
    mensagem.attach(MIMEText(corpo, 'plain'))

    # Tentando enviar o e-mail
    try:
        with smtplib.SMTP(servidor_smtp, porta_smtp) as servidor:
            servidor.starttls()
            servidor.login(remetente, senha)
            servidor.send_message(mensagem)
        return True  # Indica que o e-mail foi enviado com sucesso
    except Exception as e:
        print(f"Erro ao enviar e-mail: {e}")
        return False  # Indica que houve um erro ao enviar o e-mail

# Refatoração
class ConfiguracoesEmail:
    SERVIDOR_SMTP = 'smtp.gmail.com'
    PORTA_SMTP = 587
    SEU_EMAIL = 'seu_email@gmail.com'  # Substitua pelo seu e-mail
    SUA_SENHA = 'sua_senha'  # Substitua pela sua senha

def enviar_email(destinatario, assunto, corpo):
    mensagem = MIMEMultipart()
    mensagem['From'] = ConfiguracoesEmail.SEU_EMAIL
    mensagem['To'] = destinatario
    mensagem['Subject'] = assunto
    mensagem.attach(MIMEText(corpo, 'plain'))

    try:
        with smtplib.SMTP(ConfiguracoesEmail.SERVIDOR_SMTP, ConfiguracoesEmail.PORTA_SMTP) as servidor:
            servidor.starttls()
            servidor.login(ConfiguracoesEmail.SEU_EMAIL, ConfiguracoesEmail.SUA_SENHA)
            servidor.send_message(mensagem)
        return True  # Indica que o e-mail foi enviado com sucesso
    except Exception as e:
        print(f"Erro ao enviar e-mail: {e}")
        return False  # Indica que houve um erro ao enviar o e-mail
"""
# Ciclo 03:
# Teste
class TestEmailVerification(unittest.TestCase):
    # Ciclo 1

    def test_verificar_email_existente(self):
        resultado = verificar_email('usuario_existente@example.com')
        self.assertTrue(resultado)

    # Ciclo 2

    def test_verificar_email_com_sucesso(self):
        resultado = enviar_email('usuario@example.com', 'Assunto', 'Corpo do email')
        self.assertTrue(resultado)

    # Ciclo 3

    def test_enviar_email_com_sucesso(self):
        resultado = verificar_email('usuario_existente@example.com')
        self.assertTrue(resultado)

# Implementação
def verificar_email(email):
    # Implementação real para verificar se o email existe
    return True

def enviar_email(destinatario, assunto, corpo):
    # Implementação real para enviar o e-mail
    return True
